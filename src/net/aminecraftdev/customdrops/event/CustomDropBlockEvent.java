package net.aminecraftdev.customdrops.event;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 14-Sep-18
 */
public class CustomDropBlockEvent extends CustomDropEvent {

    private static final HandlerList HANDLERS = new HandlerList();

    @Getter private final Material material;
    @Getter private final Block block;

    public CustomDropBlockEvent(Block block, Material material, List<ItemStack> drops, List<String> commands, int exp) {
        super(drops, commands, exp);

        this.material = material;
        this.block = block;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
