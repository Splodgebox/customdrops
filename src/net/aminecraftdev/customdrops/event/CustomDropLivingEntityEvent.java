package net.aminecraftdev.customdrops.event;

import lombok.Getter;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class CustomDropLivingEntityEvent extends CustomDropEvent {

    private static final HandlerList HANDLERS = new HandlerList();

    @Getter private final LivingEntity livingEntity;
    @Getter private final EntityType entityType;

    public CustomDropLivingEntityEvent(LivingEntity livingEntity, EntityType entityType, List<ItemStack> drops, List<String> commands, int exp) {
        super(drops, commands, exp);

        this.livingEntity = livingEntity;
        this.entityType = entityType;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
