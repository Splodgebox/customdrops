package net.aminecraftdev.customdrops.event;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class CustomDropEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();

    @Getter @Setter private List<ItemStack> customDrops;
    @Getter @Setter private int exp, multiplied = 1;
    @Getter @Setter private List<String> commands;

    public CustomDropEvent(List<ItemStack> drops, List<String> commands, int exp) {
        this.customDrops = drops;
        this.commands = commands;
        this.exp = exp;
    }

    public void addCustomDrop(ItemStack itemStack) {
        this.customDrops.add(itemStack);
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
