package net.aminecraftdev.customdrops.commands;

import net.aminecraftdev.customdrops.CustomDrops;
import net.aminecraftdev.customdrops.manager.DebugManager;
import net.aminecraftdev.customdrops.manager.GUIManager;
import net.aminecraftdev.customdrops.utils.Message;
import net.aminecraftdev.customdrops.utils.command.CommandService;
import net.aminecraftdev.customdrops.utils.command.attributes.Description;
import net.aminecraftdev.customdrops.utils.command.attributes.Name;
import net.aminecraftdev.customdrops.utils.command.attributes.Permission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 24-Oct-17
 */
@Name("customdrops")
@Description("Used to reload the CustomDrops plugin")
@Permission("customdrops.admin")
public class CustomDropsCmd extends CommandService<CommandSender> {

    private CustomDrops plugin = CustomDrops.getInstance();
    private DebugManager debugManager = new DebugManager();
    private GUIManager guiManager;

    public CustomDropsCmd(GUIManager guiManager) {
        super(CustomDropsCmd.class);

        this.guiManager = guiManager;
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(args.length == 0 && commandSender instanceof Player) {
            this.guiManager.openFor((Player) commandSender);
            return;
        }

        if(args.length == 1 && args[0].equalsIgnoreCase("debug") && commandSender instanceof Player) {
            Player player = (Player) commandSender;
            String toggled;

            if(this.debugManager.isToggled(player.getUniqueId())) {
                this.debugManager.togglePlayerOff(player.getUniqueId());
                toggled = "Off";
            } else {
                this.debugManager.togglePlayerOn(player.getUniqueId());
                toggled = "On";
            }

            Message.COMMAND_CUSTOMDROPS_DEBUGTOGGLED.msg(player, toggled);
            return;
        }

        if(args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            this.plugin.reload();

            Message.COMMAND_CUSTOMDROPS_RELOADED.msg(commandSender);
            return;
        }
    }
}
