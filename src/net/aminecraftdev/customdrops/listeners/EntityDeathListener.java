package net.aminecraftdev.customdrops.listeners;

import net.aminecraftdev.customdrops.event.CustomDropEvent;
import net.aminecraftdev.customdrops.event.CustomDropLivingEntityEvent;
import net.aminecraftdev.customdrops.manager.CustomDropsManager;
import net.aminecraftdev.customdrops.storage.DataStorage;
import net.aminecraftdev.customdrops.utils.AbstractHolder;
import net.aminecraftdev.customdrops.utils.Debug;
import net.aminecraftdev.customdrops.utils.PluginUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charles on 23/2/2017.
 */
public class EntityDeathListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(EntityDeathEvent event) {
        DataStorage<EntityType> mobData = CustomDropsManager.getMobData();
        EntityType entityType = event.getEntityType();
        AbstractHolder<?> abstractHolder = mobData.getAbstractHolder(entityType);

        if(abstractHolder == null) {
            if(mobData.getAllDrop() == null) return;

            abstractHolder = mobData.getAllDrop();
        }

        Player killer = event.getEntity().getKiller();
        List<ItemStack> customDrops = null;
        List<String> commands = null;

        if(killer != null) {
            commands = mobData.getListOfCustomCommands(abstractHolder);
            commands.replaceAll(s -> s.replace("{p}", killer.getName()));

            if(PluginUtils.getItemInHand(killer).containsEnchantment(Enchantment.LOOT_BONUS_MOBS)) {
                int level = PluginUtils.getItemInHand(killer).getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS);
                int extraDrops = mobData.getDropsToAddPerLevel() * level;

                customDrops = mobData.getListOfCustomDrops(abstractHolder, extraDrops);
            }
        }

        if(customDrops == null) {
            customDrops = mobData.getListOfCustomDrops(abstractHolder);
        }

        Object exp = abstractHolder.get("exp");
        Object naturalDrops = abstractHolder.get("naturalDrops");

        if(exp instanceof Integer) {
            event.setDroppedExp((int) exp);
        }

        if(naturalDrops instanceof Boolean) {
            if((boolean) naturalDrops) {
                customDrops.addAll(event.getDrops());
            }
        }

        CustomDropEvent customDropEvent = new CustomDropLivingEntityEvent(event.getEntity(), entityType, customDrops, commands, event.getDroppedExp());
        StringBuilder stringBuilder = new StringBuilder();

        Bukkit.getPluginManager().callEvent(customDropEvent);

        List<ItemStack> totalDrops = new ArrayList<>();
        List<String> totalCommands = new ArrayList<>();
        int totalExp = 0;

        for(int i = 1; i <= customDropEvent.getMultiplied(); i++) {
            totalExp += customDropEvent.getExp();
            totalDrops.addAll(new ArrayList<>(customDropEvent.getCustomDrops()));

            if(customDropEvent.getCommands() != null) {
                totalCommands.addAll(new ArrayList<>(customDropEvent.getCommands()));
            }
        }

        event.getDrops().clear();
        event.getDrops().addAll(totalDrops);
        event.setDroppedExp(totalExp);

        if(killer != null && !totalCommands.isEmpty()) totalCommands.replaceAll(s -> s.replace("{p}", killer.getName()));
        if(!totalCommands.isEmpty()) totalCommands.forEach(s -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s));

        event.getDrops().forEach(itemStack -> {
            if(itemStack == null || itemStack.getType() == Material.AIR) return;

            stringBuilder.append(itemStack.getAmount()).append("x ").append(itemStack.getType()).append(";; ");
        });

        Debug.SPAWN_DEBUG.debug(entityType.name(), exp, naturalDrops, stringBuilder);
    }

}
