package net.aminecraftdev.customdrops.listeners;

import net.aminecraftdev.customdrops.event.CustomDropBlockEvent;
import net.aminecraftdev.customdrops.event.CustomDropEvent;
import net.aminecraftdev.customdrops.manager.CustomDropsManager;
import net.aminecraftdev.customdrops.storage.DataStorage;
import net.aminecraftdev.customdrops.utils.AbstractHolder;
import net.aminecraftdev.customdrops.utils.Debug;
import net.aminecraftdev.customdrops.utils.PluginUtils;
import net.aminecraftdev.customdrops.utils.ServerUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 14-Sep-18
 */
public class BlockBreakListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBreak(BlockBreakEvent event) {
        if(event.isCancelled()) return;

        DataStorage<String> blockData = CustomDropsManager.getBlockData();
        Block block = event.getBlock();
        Material material = block.getType();
        int meta = (int) block.getData();
        AbstractHolder<?> abstractHolder = blockData.getAbstractHolder(material.name() + ":" + meta);

        if(abstractHolder == null) {
            abstractHolder = blockData.getAbstractHolder(material.getId() + ":" + meta);
        }

        if(abstractHolder == null) {
            abstractHolder = blockData.getAbstractHolder(material.getId() + "");
        }

        if(abstractHolder == null) {
            abstractHolder = blockData.getAbstractHolder(material.name());
        }

        if(abstractHolder == null) {
            if(blockData.getAllDrop() == null) return;

            abstractHolder = blockData.getAllDrop();
        }

        if(abstractHolder == null) return;

        Player player = event.getPlayer();
        ItemStack hand = PluginUtils.getItemInHand(player);
        List<ItemStack> customDrops = blockData.getListOfCustomDrops(abstractHolder);
        List<String> commands = blockData.getListOfCustomCommands(abstractHolder);

        if(player != null) {
            if(player.getGameMode() == GameMode.CREATIVE) return;

            commands = blockData.getListOfCustomCommands(abstractHolder);
            commands.replaceAll(s -> s.replace("{p}", player.getName()));

            if(hand.getType().name().contains("PICKAXE")) {
                if(hand.containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
                    int level = hand.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
                    int extraDrops = blockData.getDropsToAddPerLevel() * level;

                    customDrops = blockData.getListOfCustomDrops(abstractHolder, extraDrops);
                }
            }
        }

        Object exp = abstractHolder.get("exp");
        Object naturalDrops = abstractHolder.get("naturalDrops");

        if(exp instanceof Integer) {
            event.setExpToDrop((int) exp);
        }

        CustomDropEvent customDropEvent = new CustomDropBlockEvent(block, material, customDrops, commands, event.getExpToDrop());
        StringBuilder stringBuilder = new StringBuilder();

        Bukkit.getPluginManager().callEvent(customDropEvent);

        List<ItemStack> totalDrops = new ArrayList<>();
        List<String> totalCommands = new ArrayList<>();
        int totalExp = 0;

        for(int i = 1; i <= customDropEvent.getMultiplied(); i++) {
            totalExp += customDropEvent.getExp();
            totalDrops.addAll(new ArrayList<>(customDropEvent.getCustomDrops()));

            if(customDropEvent.getCommands() != null) {
                totalCommands.addAll(new ArrayList<>(customDropEvent.getCommands()));
            }
        }

        if(naturalDrops instanceof Boolean) {
            if(!(boolean) naturalDrops) {
                event.getBlock().setType(Material.AIR);
            }
        }

        event.setExpToDrop(totalExp);
        ServerUtils.runTaskLater(1L, () -> PluginUtils.dropItems(event.getBlock().getLocation().add(0.5, 0.5, 0.5), totalDrops));

        if(player != null && !totalCommands.isEmpty()) totalCommands.replaceAll(s -> s.replace("{p}", player.getName()));
        if(!totalCommands.isEmpty()) totalCommands.forEach(s -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s));

        totalDrops.forEach(itemStack -> {
            if(itemStack == null || itemStack.getType() == Material.AIR) return;

            stringBuilder.append(itemStack.getAmount()).append("x ").append(itemStack.getType()).append(";; ");
        });

        Debug.SPAWN_DEBUG.debug(material.name(), exp, naturalDrops, stringBuilder.toString());
    }
}
