package net.aminecraftdev.customdrops.storage;

import lombok.Getter;
import lombok.Setter;
import net.aminecraftdev.customdrops.CustomDrops;
import net.aminecraftdev.customdrops.manager.CustomDropsManager;
import net.aminecraftdev.customdrops.utils.*;
import net.aminecraftdev.customdrops.utils.factory.SkullFactory;
import net.aminecraftdev.customdrops.utils.file.types.YmlQFile;
import net.aminecraftdev.customdrops.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.customdrops.utils.message.MessageUtils;
import net.aminecraftdev.customdrops.utils.mob.MobTypes;
import net.aminecraftdev.customdrops.utils.panel.Panel;
import net.aminecraftdev.customdrops.utils.panel.builder.PanelBuilder;
import net.aminecraftdev.customdrops.utils.panel.builder.PanelBuilderCounter;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Sep-18
 */
@SuppressWarnings("unchecked")
public class DataStorage<StorageType> {

    @Getter private final Set<AbstractHolder<StorageType>> Data = new HashSet<>();

    @Getter private AbstractHolder<Object> AllDrop = null;

    @Getter @Setter private boolean applyExtraDrops;
    @Getter @Setter private int dropsToAddPerLevel;

    private SkullFactory skullFactory;

    public void loadPage(Panel panel) {
        List<AbstractHolder<StorageType>> dataClone = new ArrayList<>(Data);
        int maxPage = (int) Math.ceil((double) dataClone.size() / (double) panel.getPanelBuilderSettings().getFillTo()) - 1;

        panel.setOnPageChange((player, currentPage, requestedPage) -> {
            if(requestedPage < 0 || requestedPage > maxPage) return false;

            loadPage(requestedPage, panel, dataClone);
            return true;
        });

        loadPage(0, panel, dataClone);
    }

    public AbstractHolder<StorageType> getAbstractHolder(StorageType storageType) {
        return Data.stream().filter(holder -> holder.getIdentifier().equals(storageType)).findFirst().orElse(null);
    }

    public List<String> getListOfCustomCommands(AbstractHolder<?> abstractHolder) {
        return getListOfCustomCommands(abstractHolder, 0);
    }

    public List<String> getListOfCustomCommands(AbstractHolder<?> abstractHolder, int extraDrops) {
        List<String> commands = new ArrayList<>();

        if(abstractHolder == null) return commands;

        Object drops = abstractHolder.get("drops");

        if(drops != null) {
            List<ConfigurationSection> dropSections = (List<ConfigurationSection>) drops;

            dropSections.forEach(innerSection -> {
                if(!innerSection.contains("commands")) return;

                int amount = getAmountOfItemStack(innerSection);

                if(amount == 0) return;
                if(!canExecute(innerSection)) return;

                List<String> standardCommands = innerSection.getStringList("commands");

                commands.addAll(standardCommands);
            });
        }

        return commands;
    }

    public List<ItemStack> getListOfCustomDrops(AbstractHolder<?> abstractHolder) {
        return getListOfCustomDrops(abstractHolder, 0);
    }

    public List<ItemStack> getListOfCustomDrops(AbstractHolder<?> abstractHolder, int extraDrops) {
        List<ItemStack> customDropsList = new ArrayList<>();

        if(abstractHolder == null) return customDropsList;

        customDropsList.add(new ItemStack(Material.AIR));

        Object drops = abstractHolder.get("drops");

        if(drops != null) {
            List<ConfigurationSection> dropSections = (List<ConfigurationSection>) drops;

            dropSections.forEach(innerSection -> {
                if(!innerSection.contains("type")) return;

                int amount = getAmountOfItemStack(innerSection);

                if(amount == 0) return;
                if(!canExecute(innerSection)) return;

                customDropsList.add(ItemStackUtils.createItemStack(innerSection, amount+extraDrops, null));
            });
        }

        return customDropsList;
    }

    public List<ItemStack> getListOfDisplayCustomDrops(AbstractHolder<?> abstractHolder) {
        List<ItemStack> customDropsList = new ArrayList<>();

        if(abstractHolder == null) return customDropsList;

        customDropsList.add(new ItemStack(Material.AIR));

        Object drops = abstractHolder.get("drops");

        if(drops != null) {
            List<ConfigurationSection> dropSections = (List<ConfigurationSection>) drops;

            dropSections.forEach(innerSection -> {
                int min = innerSection.getInt("amount.min", 0);
                int max = innerSection.getInt("amount.max", 0) + 1;
                double chance = 100.0;

                if(innerSection.contains("chance")) {
                    chance = innerSection.getDouble("chance");
                }

                ItemStack itemStack;

                if(innerSection.contains("type")) {
                    itemStack = ItemStackUtils.createItemStack(innerSection, 1, null);
                } else {
                    itemStack = new ItemStack(CustomDropsManager.get().getCommandDisplayType());
                }

                ItemMeta itemMeta = itemStack.getItemMeta();
                List<String> lore = itemMeta.hasLore()? new ArrayList<>(itemMeta.getLore()) : new ArrayList<>();
                List<String> additionalLore = new ArrayList<>(CustomDropsManager.get().getAdditionalDisplayLore());
                double finalChance = chance;

                if(innerSection.contains("commands")) {
                    List<String> commands = innerSection.getStringList("commands");

                    for(String s : CustomDropsManager.get().getCommandDisplayLore()) {
                        if(s.equalsIgnoreCase("{commands}")) {
                            commands.forEach(command -> lore.add(StringUtils.get().translateColor("&7") + command));
                            break;
                        }

                        lore.add(StringUtils.get().translateColor(s));
                    }

                    if(itemStack.getType() == CustomDropsManager.get().getCommandDisplayType()) {
                        itemMeta.setDisplayName(CustomDropsManager.get().getCommandDisplayName());
                    }
                }

                additionalLore.replaceAll(s -> s
                        .replace("{chance}", ""+finalChance)
                        .replace("{min}", ""+min)
                        .replace("{max}", ""+max)
                );

                lore.addAll(additionalLore);
                itemMeta.setLore(lore);
                itemStack.setItemMeta(itemMeta);

                customDropsList.add(itemStack);
            });
        }

        return customDropsList;
    }

    public void reload(SkullFactory skullFactory, ConfigurationSection lootingSection, ConfigurationSection allSection, ConfigurationSection dropsSection, boolean isMobs) {
        this.skullFactory = skullFactory;

        if(lootingSection == null) {
            this.applyExtraDrops = false;
            this.dropsToAddPerLevel = 1;
        } else {
            this.applyExtraDrops = lootingSection.getBoolean("applyToDrops", false);
            this.dropsToAddPerLevel = lootingSection.getInt("increasePerLevel", 1);
        }

        if(allSection == null) {
            AllDrop = null;
        } else {
            AllDrop = new AbstractHolder<>("all", "all");
            fillAbstractHolder(AllDrop, allSection, isMobs);
        }

        Data.clear();

        dropsSection.getKeys(false).forEach(key -> {
            key = key.toUpperCase();
            StorageType storageType;

            if(isEntity(key)) {
                storageType = (StorageType) EntityType.valueOf(key);
            } else if(isMaterial(key)) {
                storageType = (StorageType) key;
            } else {
                return;
            }

            AbstractHolder<StorageType> abstractHolder = new AbstractHolder<>(storageType, key);
            ConfigurationSection innerSection = dropsSection.getConfigurationSection(key);

            fillAbstractHolder(abstractHolder, innerSection, isMobs);
            Data.add(abstractHolder);
        });
    }

    private void loadPage(int page, Panel panel, List<AbstractHolder<StorageType>> list) {
        ServerUtils.runTaskAsync(() -> {
            int startIndex = page * panel.getPanelBuilderSettings().getFillTo();

            for(int i = startIndex; i < startIndex + panel.getPanelBuilderSettings().getFillTo(); i++) {
                if(i >= list.size()) {
                    panel.setItem(i-startIndex, new ItemStack(Material.AIR), e -> {});
                } else {
                    AbstractHolder<StorageType> abstractHolder = list.get(i);
                    String type = abstractHolder.getPrimaryKey();
                    ItemStack itemStack;

                    if(isMaterial(type)) {
                        itemStack = new ItemStack(ItemStackUtils.getType(type));
                        itemStack.setDurability((short) ItemStackUtils.getMeta(type));
                    } else if(isEntity(type)) {
                        EntityType entityType = EntityType.valueOf(type);

                        itemStack = this.skullFactory.getMobSkull(MobTypes.getMobType(entityType));
                    } else {
                        continue;
                    }

                    ItemMeta itemMeta = itemStack.getItemMeta();

                    itemMeta.setDisplayName(MessageUtils.translateString("&e&l" + StringUtils.get().formatString(type)));
                    itemStack.setItemMeta(itemMeta);

                    panel.setItem(i-startIndex, itemStack, event -> openEditorPage(abstractHolder, itemStack, (Player) event.getWhoClicked()));
                }
            }
        });
    }

    private void openEditorPage(AbstractHolder<StorageType> abstractHolder, ItemStack displayClone, Player player) {
        ConfigurationSection section = CustomDrops.getInstance().getEditor().getConfigurationSection("EditorMainPanel");
        Map<String, String> replaceMap = new HashMap<>();
        String naturalDrops = abstractHolder.get("naturalDrops") == null? "enabled" : (boolean) abstractHolder.get("naturalDrops")? "enabled" : "disabled";
        String expAmount = abstractHolder.get("exp") == null? "0" : StringUtils.get().formatNumber((int) abstractHolder.get("exp"));

        replaceMap.put("{naturalDrops}", naturalDrops);
        replaceMap.put("{exp}", expAmount);

        PanelBuilder panelBuilder = new PanelBuilder(section, replaceMap);

        panelBuilder.getPanelBuilderCounter()
                .addSlotCounter("Protected")
                .addSlotCounter("Display");

        Panel panel = panelBuilder.getPanel()
                .setCancelClick(true)
                .setCancelLowerClick(true)
                .setDestroyWhenDone(true);
        PanelBuilderCounter panelBuilderCounter = panelBuilder.getPanelBuilderCounter();
        List<ItemStack> currentDrops = getListOfDisplayCustomDrops(abstractHolder);
        int fillToSlot = panel.getPanelBuilderSettings().getFillTo();
        int currentSlot = 0;

        panelBuilderCounter.getSlotsWith("Display").forEach(slot -> panel.setItem(slot, displayClone.clone(), event -> event.setCancelled(true)));
        panelBuilderCounter.getSlotsWith("Protected").forEach(slot -> panel.setOnClick(slot, event -> event.setCancelled(true)));

        for(ItemStack itemStack : currentDrops) {
            if(currentSlot >= fillToSlot) break;
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            panel.setItem(currentSlot, itemStack);
            currentSlot += 1;
        }

        panel.openFor(player);
    }

    private int getAmountOfItemStack(ConfigurationSection configurationSection) {
        int min = configurationSection.getInt("amount.min", 0);
        int max = configurationSection.getInt("amount.max", 0) + 1;

        return RandomUtils.getRandomInt(max - min) + min;
    }

    private boolean canExecute(ConfigurationSection innerSection) {
        if(innerSection.contains("chance")) {
            double chance = innerSection.getDouble("chance");
            double randomNumbr = RandomUtils.getRandomInt(100);

            randomNumbr += RandomUtils.getRandom().nextDouble();

            if(randomNumbr > chance) return false;
        }

        return true;
    }

    private boolean isEntity(String name) {
        try {
            EntityType.valueOf(name);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    private boolean isMaterial(String name) {
        try {
            Material mat = ItemStackUtils.getType(name);

            return (mat != null);
        } catch (Exception ex) {
            return false;
        }
    }

    private void fillAbstractHolder(AbstractHolder<?> abstractHolder, ConfigurationSection innerSection, boolean isMobs) {
        innerSection.getKeys(false).forEach(s -> {
            if(s.equalsIgnoreCase("EXP")) {
                abstractHolder.set("exp", innerSection.getInt(s, 10));
            } else if(s.equalsIgnoreCase("NaturalDrops")) {
                abstractHolder.set("naturalDrops", innerSection.getBoolean(s, true));
            } else if(s.equalsIgnoreCase("meta")) {
                abstractHolder.set("meta", innerSection.getInt("meta", 0));
            } else {
                List<ConfigurationSection> dropSections = abstractHolder.get("drops") == null? new ArrayList<>() : (List<ConfigurationSection>) abstractHolder.get("drops");
                ConfigurationSection numberDropSection = innerSection.getConfigurationSection(s);

                dropSections.add(numberDropSection);
                abstractHolder.set("drops", dropSections);
            }
        });

        abstractHolder.set("isMobs", isMobs);
        abstractHolder.set("innerSectionPath", innerSection.getCurrentPath());
    }

}
