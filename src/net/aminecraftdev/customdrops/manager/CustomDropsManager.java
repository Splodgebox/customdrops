package net.aminecraftdev.customdrops.manager;

import lombok.Getter;
import net.aminecraftdev.customdrops.CustomDrops;
import net.aminecraftdev.customdrops.storage.DataStorage;
import net.aminecraftdev.customdrops.utils.Reloadable;
import net.aminecraftdev.customdrops.utils.StringUtils;
import net.aminecraftdev.customdrops.utils.factory.SkullFactory;
import net.aminecraftdev.customdrops.utils.itemstack.ItemStackUtils;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 22-Feb-18
 */
public class CustomDropsManager implements Reloadable {

    @Getter private static final DataStorage<String> BlockData = new DataStorage<>();
    @Getter private static final DataStorage<EntityType> MobData = new DataStorage<>();

    private static CustomDrops PLUGIN = CustomDrops.getInstance();
    private static CustomDropsManager instance;

    @Getter private List<String> additionalDisplayLore, commandDisplayLore;
    @Getter private Material commandDisplayType;
    @Getter private String commandDisplayName;
    private SkullFactory skullFactory;

    public CustomDropsManager(SkullFactory skullFactory) {
        instance = this;

        this.skullFactory = skullFactory;
        reload();
    }

    @Override
    public void reload() {
        ConfigurationSection mobs = PLUGIN.getConfig().getConfigurationSection("Mobs");
        ConfigurationSection blocks = PLUGIN.getConfig().getConfigurationSection("Blocks");

        MobData.reload(this.skullFactory, mobs.getConfigurationSection("Looting"), mobs.getConfigurationSection("All"), PLUGIN.getMobs().getConfigurationSection("CustomDrops"), true);
        BlockData.reload(this.skullFactory, blocks.getConfigurationSection("Fortune"), blocks.getConfigurationSection("All"), PLUGIN.getBlocks().getConfigurationSection("CustomDrops"), false);

        this.additionalDisplayLore = PLUGIN.getConfig().getStringList("AdditionalDisplayLore");
        this.commandDisplayType = ItemStackUtils.getType(PLUGIN.getConfig().getString("CommandDisplay.type"));
        this.commandDisplayName = StringUtils.get().translateColor(PLUGIN.getConfig().getString("CommandDisplay.name"));
        this.commandDisplayLore = PLUGIN.getConfig().getStringList("CommandDisplay.lore");

        this.additionalDisplayLore.replaceAll(s -> StringUtils.get().translateColor(s));
    }

    public static CustomDropsManager get() {
        return instance;
    }

}
