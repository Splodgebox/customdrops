package net.aminecraftdev.customdrops.manager;

import net.aminecraftdev.customdrops.CustomDrops;
import net.aminecraftdev.customdrops.panels.BlocksPanel;
import net.aminecraftdev.customdrops.panels.MainPanel;
import net.aminecraftdev.customdrops.panels.MobsPanel;
import net.aminecraftdev.customdrops.utils.panel.IPanelHandler;
import net.aminecraftdev.customdrops.utils.panel.Panel;
import net.aminecraftdev.customdrops.utils.panel.builder.PanelBuilder;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 29-Jul-18
 */
public class GUIManager {

    private IPanelHandler mainPanel, mobPanel, blockPanel;

    public GUIManager(CustomDrops customDrops) {
        FileConfiguration editor = customDrops.getEditor();
        PanelBuilder mainPanelBuilder = new PanelBuilder(editor.getConfigurationSection("MainPanel"));
        PanelBuilder mobPanelBuilder = new PanelBuilder(editor.getConfigurationSection("MobPanel"));
        PanelBuilder blockPanelBuilder = new PanelBuilder(editor.getConfigurationSection("BlockPanel"));

        this.mainPanel = new MainPanel(this);
        this.mobPanel = new MobsPanel(this);
        this.blockPanel = new BlocksPanel(this);

        this.mainPanel.load(mainPanelBuilder);
        this.mobPanel.load(mobPanelBuilder);
        this.blockPanel.load(blockPanelBuilder);
    }

    public void openFor(Player player) {
        this.mainPanel.openFor(player);
    }

    public void openBlockPanel(Player player) {
        Panel clone = this.blockPanel.getPanel().clone().setDestroyWhenDone(true);
        clone.setParentPanel(this.mainPanel.getPanel());

        CustomDropsManager.getBlockData().loadPage(clone);

        clone.setOnClose(event -> updateMainBlockPanel());
        clone.openFor(player);
    }

    public void openMobPanel(Player player) {
        Panel clone = this.mobPanel.getPanel().clone().setDestroyWhenDone(true);
        clone.setParentPanel(this.mainPanel.getPanel());

        CustomDropsManager.getMobData().loadPage(clone);

        clone.setOnClose(event -> updateMainMobPanel());
        clone.openFor(player);
    }

    private void updateMainBlockPanel() {

    }

    private void updateMainMobPanel() {

    }

}