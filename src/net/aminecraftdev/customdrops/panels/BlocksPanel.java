package net.aminecraftdev.customdrops.panels;

import lombok.Getter;
import net.aminecraftdev.customdrops.manager.GUIManager;
import net.aminecraftdev.customdrops.utils.panel.IPanelHandler;
import net.aminecraftdev.customdrops.utils.panel.Panel;
import net.aminecraftdev.customdrops.utils.panel.builder.PanelBuilder;
import org.bukkit.entity.Player;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 14-Sep-18
 */
public class BlocksPanel implements IPanelHandler {

    @Getter private final GUIManager guiManager;
    @Getter private Panel panel;

    public BlocksPanel(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void load(PanelBuilder panelBuilder) {
        this.panel = panelBuilder.getPanel()
                .setDestroyWhenDone(false)
                .setCancelLowerClick(true)
                .setCancelClick(true);
    }

    @Override
    public void openFor(Player player) {
        this.panel.openFor(player);
    }

}
