package net.aminecraftdev.customdrops.panels;

import lombok.Getter;
import net.aminecraftdev.customdrops.manager.GUIManager;
import net.aminecraftdev.customdrops.utils.panel.IPanelHandler;
import net.aminecraftdev.customdrops.utils.panel.Panel;
import net.aminecraftdev.customdrops.utils.panel.builder.PanelBuilder;
import org.bukkit.entity.Player;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 14-Sep-18
 */
public class MainPanel implements IPanelHandler {

    @Getter private final GUIManager guiManager;
    @Getter private Panel panel;

    public MainPanel(GUIManager guiManager) {
        this.guiManager = guiManager;
    }

    @Override
    public void load(PanelBuilder panelBuilder) {
        panelBuilder.getPanelBuilderCounter()
                .addSlotCounter("Blocks")
                .addSlotCounter("Mobs");

        this.panel = panelBuilder.getPanel()
                .setDestroyWhenDone(false)
                .setCancelLowerClick(true)
                .setCancelClick(true);

        panelBuilder.getPanelBuilderCounter().getSlotsWith("Blocks").forEach(slot -> this.panel.setOnClick(slot, event -> this.guiManager.openBlockPanel((Player) event.getWhoClicked())));
        panelBuilder.getPanelBuilderCounter().getSlotsWith("Mobs").forEach(slot -> this.panel.setOnClick(slot, event -> this.guiManager.openMobPanel((Player) event.getWhoClicked())));
    }

    @Override
    public void openFor(Player player) {
        this.panel.openFor(player);
    }

}
