package net.aminecraftdev.customdrops.utils;

import org.bukkit.ChatColor;

import java.text.DecimalFormat;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Apr-18
 */
public class StringUtils {

    private static StringUtils INSTANCE = new StringUtils();

    private DecimalFormat numberFormat = new DecimalFormat("###,###,###,###,###,###.##");

    public String stripColor(String string) {
        return ChatColor.stripColor(string);
    }

    public String translateColor(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    public String formatString(String string) {
        string = string.toLowerCase();

        StringBuilder stringBuilder = new StringBuilder();

        if(string.contains(" ")) {
            for(String z : string.split(" ")) {
                stringBuilder.append(Character.toUpperCase(z.charAt(0))).append(z.substring(1).toLowerCase());
            }
        } else if(string.contains("_")) {
            String[] split = string.split("_");

            for(int i = 0; i < split.length; i++) {
                String z = split[i];

                stringBuilder.append(Character.toUpperCase(z.charAt(0))).append(z.substring(1).toLowerCase());

                if(i != (split.length - 1)) {
                    stringBuilder.append(" ");
                }
            }
        } else {
            return Character.toUpperCase(string.charAt(0)) + string.substring(1).toLowerCase();
        }

        return stringBuilder.toString();
    }

    public String formatNumber(long input) {
        return this.numberFormat.format(input);
    }

    public String formatNumber(double input) {
        return this.numberFormat.format(input);
    }

    public static StringUtils get() {
        return INSTANCE;
    }

}
