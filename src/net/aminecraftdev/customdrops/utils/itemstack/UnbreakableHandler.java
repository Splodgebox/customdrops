package net.aminecraftdev.customdrops.utils.itemstack;

import net.aminecraftdev.customdrops.utils.IHandler;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Dec-18
 */
public class UnbreakableHandler implements IHandler<ItemMeta, ItemMeta> {

    @Override
    public ItemMeta handle(ItemMeta handler) {
        handler.setUnbreakable(true);

        return handler;
    }
}
