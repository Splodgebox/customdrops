package net.aminecraftdev.customdrops.utils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 17-Nov-17
 */
public interface Reloadable {

    void reload();

}
