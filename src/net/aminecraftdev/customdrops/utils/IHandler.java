package net.aminecraftdev.customdrops.utils;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Dec-18
 */
public interface IHandler<T, ReturnType> {

    ReturnType handle(T handler);

}
