package net.aminecraftdev.customdrops.utils;

import net.aminecraftdev.customdrops.manager.DebugManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jul-18
 */
public enum Debug {

    SPAWN_DEBUG("--------[ {0} Custom Drops ]--------" +
            "\nEXP: {1}" +
            "\nNatural Drops: {2}" +
            "\nDrops: {3}");

    private static DebugManager debugManager = new DebugManager();

    private String message;

    Debug(String message) {
        this.message = message;
    }

    public void debug(Object... objects) {
        int current = 0;
        String message = this.message;

        for(Object object : objects) {
            String placeholder = "{" + current + "}";

            if(object == null) {
                message = message.replace(placeholder, "-");
            } else {
                message = message.replace(placeholder, object.toString());
            }

            current += 1;
        }

        String msg = message;

        debugManager.getToggledPlayers().forEach(uuid -> {
            Player player = Bukkit.getPlayer(uuid);

            if(player == null) {
                debugManager.togglePlayerOff(uuid);
                return;
            }

            player.sendMessage(msg);
        });
    }
}
