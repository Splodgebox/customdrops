package net.aminecraftdev.customdrops.utils.factory;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.aminecraftdev.customdrops.utils.mob.MobTypes;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.UUID;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 25-Jul-18
 */
public class SkullFactory {

    public ItemStack getMobSkull(MobTypes mobTypes) {
        String encodedTexture = mobTypes.getTexture();

        if(encodedTexture == null) return null;

        if(encodedTexture.equalsIgnoreCase("[vanilla]")) {
            return getVanillaSkull(mobTypes);
        }

        SkullMeta skullMeta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
        Field field;

        gameProfile.getProperties().put("textures", new Property("textures", encodedTexture));

        try {
            field = skullMeta.getClass().getDeclaredField("profile");
            field.setAccessible(true);
            field.set(skullMeta, gameProfile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
            ex.printStackTrace();
            return null;
        }

        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);

        skull.setItemMeta(skullMeta);
        return skull;
    }

    public ItemStack getVanillaSkull(MobTypes mobTypes) {
        int code;

        switch (mobTypes) {
            case Ender_Dragon:
                code = 5;
                break;
            case Zombie:
                code = 2;
                break;
            case Skeleton:
                code = 0;
                break;
            case Creeper:
                code = 4;
                break;
            default:
                return null;
        }

        return new ItemStack(Material.SKULL_ITEM, 1, (short) code);
    }

}