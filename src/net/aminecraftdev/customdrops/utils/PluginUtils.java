package net.aminecraftdev.customdrops.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Jul-17
 */
public class PluginUtils {

    private static String version = NMSVersionHandler.getAPIVersion();
    private static boolean useOldVersion = version.startsWith("v1_8") || version.startsWith("v1_7");

    public static boolean checkPlugin(String pluginName, String requiredPluginName, String requiredPluginLink) {
        Plugin plugin = Bukkit.getPluginManager().getPlugin(requiredPluginName);

        if(plugin == null) {
            ServerUtils.logError("Dependency missing, " + pluginName + " requires plugin " + requiredPluginName + " which can be downloaded from: " + requiredPluginLink);
            return false;
        }

        return true;
    }

    public static ItemStack getItemInHand(Player player) {
        if(useOldVersion) {
            return player.getItemInHand();
        } else {
            return player.getInventory().getItemInMainHand();
        }
    }

    public static void setItemInHand(Player player, ItemStack itemStack) {
        if(useOldVersion) {
            player.setItemInHand(itemStack);
        } else {
            player.getInventory().setItemInMainHand(itemStack);
        }
    }

    public static void dropItems(Location location, ItemStack... itemStacks) {
        dropItems(location, Arrays.asList(itemStacks));
    }

    public static void dropItems(Location location, Collection<ItemStack> drops) {
        drops.forEach(drop -> {
            if(drop == null || drop.getType() == Material.AIR) return;

            location.getWorld().dropItemNaturally(location, drop);
        });
    }

}
