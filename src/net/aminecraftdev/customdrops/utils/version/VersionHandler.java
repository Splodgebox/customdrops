package net.aminecraftdev.customdrops.utils.version;

import lombok.Getter;
import net.aminecraftdev.customdrops.utils.Versions;
import net.aminecraftdev.customdrops.utils.itemstack.UnbreakableHandler;
import net.aminecraftdev.customdrops.utils.mob.LlamaHandler;
import net.aminecraftdev.customdrops.utils.mob.ParrotHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Dec-18
 */
public class VersionHandler {

    private static final VersionHandler instance = new VersionHandler();

    @Getter private UnbreakableHandler unbreakableHandler;
    @Getter private ParrotHandler parrotHandler;
    @Getter private LlamaHandler llamaHandler;
    @Getter private Versions version;

    private VersionHandler() {
        String v = Bukkit.getServer().getClass().getPackage().getName();

        v = v.substring(v.lastIndexOf(".") + 1);

        this.version = Versions.getVersion(v);

        if(this.version.isHigherThanOrEqualTo(Versions.v1_11_R1)) {
            this.llamaHandler = new LlamaHandler();
        }
        if(this.version.isHigherThanOrEqualTo(Versions.v1_12_R1)) {
            this.parrotHandler = new ParrotHandler();
        }
        if(this.version.isHigherThanOrEqualTo(Versions.v1_9_R1)) {
            this.unbreakableHandler = new UnbreakableHandler();
        }
    }

    public boolean canUseOffHand() {
        return this.version.isHigherThanOrEqualTo(Versions.v1_9_R1);
    }

    public ItemStack getItemInHand(HumanEntity humanEntity) {
        if(this.version.isLessThanOrEqualTo(Versions.v1_8_R3)) {
            return humanEntity.getItemInHand();
        } else {
            return humanEntity.getInventory().getItemInMainHand();
        }
    }

    public void setItemInHand(HumanEntity humanEntity, ItemStack itemStack) {
        if(this.version.isLessThanOrEqualTo(Versions.v1_8_R3)) {
            humanEntity.setItemInHand(itemStack);
        } else {
            humanEntity.getInventory().setItemInMainHand(itemStack);
        }
    }

    public static VersionHandler get() {
        return instance;
    }

}
