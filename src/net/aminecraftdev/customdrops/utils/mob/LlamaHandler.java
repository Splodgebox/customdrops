package net.aminecraftdev.customdrops.utils.mob;

import net.aminecraftdev.customdrops.utils.IHandler;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Llama;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Dec-18
 */
public class LlamaHandler implements IHandler<LivingEntity, MobTypes> {

    @Override
    public MobTypes handle(LivingEntity livingEntity) {
        Llama llama = (Llama) livingEntity;

        if (llama.getColor() == null) return null;

        switch (llama.getColor()) {
            case BROWN:
                return MobTypes.Brown_Llama;
            case CREAMY:
                return MobTypes.Creamy_Llama;
            case GRAY:
                return MobTypes.Gray_Llama;
            case WHITE:
                return MobTypes.White_Llama;
        }

        return null;
    }
}
