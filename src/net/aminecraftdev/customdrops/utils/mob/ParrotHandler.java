package net.aminecraftdev.customdrops.utils.mob;

import net.aminecraftdev.customdrops.utils.IHandler;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Parrot;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Dec-18
 */
public class ParrotHandler implements IHandler<LivingEntity, MobTypes> {

    @Override
    public MobTypes handle(LivingEntity livingEntity) {
        Parrot parrot = (Parrot) livingEntity;

        if (parrot.getVariant() == null) return null;

        switch (parrot.getVariant()) {
            case BLUE:
                return MobTypes.Blue_Parrot;
            case CYAN:
                return MobTypes.Cyan_Parrot;
            case GRAY:
                return MobTypes.Gray_Parrot;
            case GREEN:
                return MobTypes.Green_Parrot;
            case RED:
                return MobTypes.Red_Parrot;
        }

        return null;
    }
}
