package net.aminecraftdev.customdrops.utils.panel;

import net.aminecraftdev.customdrops.utils.panel.builder.PanelBuilder;
import org.bukkit.entity.Player;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 14-Sep-18
 */
public interface IPanelHandler {

    /**
     * The panel that is being handled.
     *
     * @return the instance of Panel which is being handled.
     */
    Panel getPanel();

    /**
     * This will load and/or refresh the
     * panel which is being handled.
     *
     * @param panelBuilder - the builder to reload with.
     */
    void load(PanelBuilder panelBuilder);

    /**
     * This will open the panel which is being
     * handled for the player.
     *
     * @param player - the player to open for.
     */
    void openFor(Player player);

}
