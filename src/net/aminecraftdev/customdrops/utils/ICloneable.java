package net.aminecraftdev.customdrops.utils;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Aug-18
 */
public interface ICloneable<T> {

    T clone();

}
