package net.aminecraftdev.customdrops;

import net.aminecraftdev.customdrops.commands.CustomDropsCmd;
import net.aminecraftdev.customdrops.listeners.BlockBreakListener;
import net.aminecraftdev.customdrops.listeners.EntityDeathListener;
import net.aminecraftdev.customdrops.manager.CustomDropsManager;
import net.aminecraftdev.customdrops.manager.GUIManager;
import net.aminecraftdev.customdrops.utils.Message;
import net.aminecraftdev.customdrops.utils.NMSVersionHandler;
import net.aminecraftdev.customdrops.utils.ServerUtils;
import net.aminecraftdev.customdrops.utils.factory.SkullFactory;
import net.aminecraftdev.customdrops.utils.file.FileUtils;
import net.aminecraftdev.customdrops.utils.file.QFile;
import net.aminecraftdev.customdrops.utils.file.types.YmlQFile;
import net.aminecraftdev.customdrops.utils.panel.Panel;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by Charles on 23/2/2017.
 */
public class CustomDrops extends JavaPlugin {

    private static CustomDrops instance;

    private YmlQFile config, lang, editor, blocks, mobs;
    private CustomDropsManager customDropsManager;
    private GUIManager guiManager;

    @Override
    public void onEnable() {
        instance = this;

        new Metrics(this);
        new NMSVersionHandler();
        new ServerUtils(this);
        QFile.setPlugin(this);
        Panel.setPlugin(this);

        loadFiles();
        convertData();
        createFiles();
        loadMessages();
        reloadFiles();

        this.customDropsManager = new CustomDropsManager(new SkullFactory());
        this.guiManager = new GUIManager(this);

        loadListeners();
        loadCommands();
    }

    @Override
    public void onDisable() {

    }

    public void reload() {
        reloadFiles();
        this.customDropsManager.reload();
    }

    @Override
    public FileConfiguration getConfig() {
        return this.config.getConfig();
    }

    public FileConfiguration getEditor() {
        return this.editor.getConfig();
    }

    public FileConfiguration getBlocks() {
        return this.blocks.getConfig();
    }

    public FileConfiguration getMobs() {
        return this.mobs.getConfig();
    }

    public void reloadFiles() {
        this.config.reloadFile();
        this.lang.reloadFile();
        this.blocks.reloadFile();
        this.mobs.reloadFile();
    }

    private void convertData() {
        long startMs = System.currentTimeMillis();

        if(!this.config.getFile().exists()) return;

        this.mobs.createFile();

        FileConfiguration mobs = this.mobs.getConfig();
        FileConfiguration config = this.config.getConfig();

        if(!config.contains("CustomDrops")) return;

        ConfigurationSection currentData = config.getConfigurationSection("CustomDrops");
        ConfigurationSection mobLooting = null;
        ConfigurationSection mobAll = null;

        for(String key : currentData.getKeys(false)) {
            if(key.equalsIgnoreCase("Looting")) {
                mobLooting = currentData.getConfigurationSection(key);
                continue;
            }
            if(key.equalsIgnoreCase("ALL")) {
                mobAll = currentData.getConfigurationSection(key);
                continue;
            }

            mobs.set("CustomDrops." + key, currentData.getConfigurationSection(key));
        }

        this.config.delete();
        this.config.createFile();
        this.editor.createFile();
        this.blocks.createFile();

        config.set("Mobs.All", mobAll);
        config.set("Mobs.Looting", mobLooting);
        this.config.saveFile();

        saveFiles();
        getLogger().info("[CustomDrops] All configuration data has been converted to the new system successfully in " + (System.currentTimeMillis() - startMs) + "ms.");
    }

    private void loadMessages() {
        for(Message message : Message.values()) {
            if(!this.lang.getConfig().contains(message.getPath())) {
                this.lang.getConfig().set(message.getPath(), message.getDefault());
            }
        }

        this.lang.saveFile();
        Message.setFile(this.lang.getConfig());
    }

    private void loadFiles() {
        this.config = new YmlQFile(new File(getDataFolder(), "config.yml")).saveResource(true);
        this.editor = new YmlQFile(new File(getDataFolder(), "editor.yml")).saveResource(true);
        this.blocks = new YmlQFile(new File(getDataFolder(), "blocks.yml")).saveResource(true);
        this.mobs = new YmlQFile(new File(getDataFolder(), "mobs.yml")).saveResource(true);
        this.lang = new YmlQFile(new File(getDataFolder(), "lang.yml")).saveResource(false);
    }

    private void createFiles() {
        this.config.createFile();
        this.editor.createFile();
        this.blocks.createFile();
        this.mobs.createFile();
        this.lang.createFile();
    }

    private void saveFiles() {
        this.config.saveFile();
        this.lang.saveFile();
        this.editor.saveFile();
        this.blocks.saveFile();
        this.mobs.saveFile();
    }

    private void loadListeners() {
        Bukkit.getPluginManager().registerEvents(new EntityDeathListener(), this);
        Bukkit.getPluginManager().registerEvents(new BlockBreakListener(), this);
    }

    private void loadCommands() {
        new CustomDropsCmd(this.guiManager);
    }

    public static CustomDrops getInstance() {
        return instance;
    }

}
