package net.aminecraftdev.customdrops;

import net.aminecraftdev.customdrops.manager.CustomDropsManager;
import net.aminecraftdev.customdrops.storage.DataStorage;
import net.aminecraftdev.customdrops.utils.AbstractHolder;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class CustomDropsAPI {

    /**
     * Used to easily access the custom drops that is dropped
     * from the specific EntityType.
     *
     * @param entityType - referenced entityType
     * @return list of custom drops for the entityType, or will return an empty list if none specified.
     */
    public static List<ItemStack> getCustomDrops(EntityType entityType) {
        DataStorage<EntityType> mobData = CustomDropsManager.getMobData();
        AbstractHolder<EntityType> abstractHolder = mobData.getAbstractHolder(entityType);
        List<ItemStack> drops = new ArrayList<>();

        if(abstractHolder == null) return drops;

        return mobData.getListOfCustomDrops(abstractHolder);
    }

    /**
     * Used to easily access the custom drops that is dropped
     * from the specific Material.
     *
     * @param material - referenced material
     * @return list of custom drops for the material, or will return an empty list if none specified.
     */
    public static List<ItemStack> getCustomDrops(Material material) {
        DataStorage<String> materialData = CustomDropsManager.getBlockData();
        AbstractHolder<String> abstractHolder = materialData.getAbstractHolder(material.name());
        List<ItemStack> drops = new ArrayList<>();

        if(abstractHolder == null) return drops;

        return materialData.getListOfCustomDrops(abstractHolder);
    }

    /**
     * Used to easily access the custom drops that is dropped
     * from the specific Material input.
     *
     * @param materialInput - referenced material input
     * @return list of custom drops for the material, or will return as an empty list if none specified
     */
    public static List<ItemStack> getCustomDrops(String materialInput) {
        DataStorage<String> materialData = CustomDropsManager.getBlockData();
        AbstractHolder<String> abstractHolder = materialData.getAbstractHolder(materialInput);
        List<ItemStack> drops = new ArrayList<>();

        if(abstractHolder == null) return drops;

        return materialData.getListOfCustomDrops(abstractHolder);
    }

    /**
     * Used to find out the custom exp drop amount from the
     * specified entityType.
     *
     * @param entityType - referenced entityType
     * @return an integer amount of exp that is dropped, or -1 if nothing is set.
     */
    public static int getExpDrop(EntityType entityType) {
        DataStorage<EntityType> mobData = CustomDropsManager.getMobData();
        AbstractHolder<EntityType> abstractHolder = mobData.getAbstractHolder(entityType);

        if(abstractHolder == null) return -1;

        Object exp = abstractHolder.get("exp");

        if(exp == null) return -1;

        return (int) exp;
    }

    /**
     * Used to find out the custom exp drop amount from the
     * specified material.
     *
     * @param materialInput - referenced material input
     * @return an integer amount of exp that is dropped, or -1 if nothing is set.
     */
    public static int getExpDrop(String materialInput) {
        DataStorage<String> materialData = CustomDropsManager.getBlockData();
        AbstractHolder<String> abstractHolder = materialData.getAbstractHolder(materialInput);

        if(abstractHolder == null) return -1;

        Object exp = abstractHolder.get("exp");

        if(exp == null) return -1;

        return (int) exp;
    }

    /**
     * Used to find out if the specified entityType has
     * natural drops enabled as well as customdrops.
     *
     * @param entityType - referenced entityType
     * @return a boolean value if natural drops are enabled, true if no custom drops are set, false if no natural drop setting is set for the entityType.
     */
    public static boolean getNaturalDrops(EntityType entityType) {
        DataStorage<EntityType> mobData = CustomDropsManager.getMobData();
        AbstractHolder<EntityType> abstractHolder = mobData.getAbstractHolder(entityType);

        if(abstractHolder == null) return true;

        Object naturalDrops = abstractHolder.get("naturalDrops");

        if(naturalDrops == null) return false;

        return (boolean) naturalDrops;
    }

    /**
     * Used to find out if the specified material has
     * natural drops enabled as well as customdrops.
     *
     * @param materialInput - referenced material input
     * @return a boolean value if natural drops are enabled, true if no custom drops are set, false if no natural drop setting is set for the material.
     */
    public static boolean getNaturalDrops(String materialInput) {
        DataStorage<String> materialData = CustomDropsManager.getBlockData();
        AbstractHolder<String> abstractHolder = materialData.getAbstractHolder(materialInput);

        if(abstractHolder == null) return true;

        Object naturalDrops = abstractHolder.get("naturalDrops");

        if(naturalDrops == null) return false;

        return (boolean) naturalDrops;
    }

}
